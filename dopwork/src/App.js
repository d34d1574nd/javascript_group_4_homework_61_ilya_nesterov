import React, { Component } from 'react';
import './App.css';
import ListPeopleStarWars from "./containers/ListPeopleStarWars/ListPeopleStarWars";



class App extends Component {
  render() {
    return (
      <div className="App">
          <ListPeopleStarWars />
      </div>
    );
  }
}

export default App;
