import React, {Component} from 'react';
import './StarWars.css';
import Axios from "axios";

class StarWars extends Component {

  state = {
    home: {},
  };

  componentDidUpdate(prevProps) {
    if (this.props.code) {
    if(prevProps.code !== this.props.code) {
      Axios.get(this.props.code).then(response => {
        this.setState({home: response.data});
      });
    }
  }}

  render() {
    return (
      <section className='home'>
        <p><strong>The planet where the main characters lived: <strong>{this.props.name}</strong></strong>{this.state.home.name}</p>
        <p><strong>Diameter: </strong>{this.state.home.diameter}</p>
        <p><strong>Terrain: </strong>{this.state.home.terrain}</p>
        <p><strong>Climate: </strong>{this.state.home.climate}</p>
        <p><strong>Population: </strong>{this.state.home.population}</p>
      </section>
    );
  }
}

export default StarWars;