import React, {Component} from 'react';
import './ListPeopleStarWars.css';
import axios from "axios";
import StarWars from "../../components/StarWars/StarWars";


class ListPeopleStarWars extends Component {

  state = {
    people: [],
    planet: '',
  };

  postSelectedHandler = (code) => {
    this.setState({planet: code});
  };

  componentDidMount() {
    const BASE_URL = 'https://swapi.co/api/';
    const PEOPLE_URL = 'people/';

    axios.get(BASE_URL + PEOPLE_URL).then(response => {
      this.setState({people: response.data.results})
    })
  }

  render() {
    const people = this.state.people.map((item, index) => {
      return (
        <section className='people' key={index} onClick={() => this.postSelectedHandler(item.homeworld)}>
          <p><strong>Name: </strong>{item.name}</p>
          <p><strong>Height: </strong>{item.height}</p>
          <p><strong>Mass: </strong>{item.mass}</p>
          <p><strong>Hair color: </strong>{item.hair_color}</p>
          <p><strong>Skin color: </strong>{item.skin_color}</p>
          <p><strong>Eye color: </strong>{item.eye_color}</p>
          <p><strong>Gender: </strong>{item.gender}</p>
          <button className='moreInfo'>More information</button>
        </section>
      )
    });
    return (
        <div>
          {people}
          <StarWars
            code={this.state.planet}
          />
        </div>
    );
  }
}

export default ListPeopleStarWars;