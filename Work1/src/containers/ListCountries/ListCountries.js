import React, {Component} from 'react';
import './ListCountries.css';
import axios from "axios";
import InfoCountry from "../../components/InfoCountry/InfoCountry";
import Country from "../../components/Country/Country";


class ListCountries extends Component {
  state = {
    countries: [],
    selectedCountryCode: null
  };

  postSelectedHandler = (code) => {
    this.setState({selectedCountryCode: code});
  };


  componentDidMount() {
    const ALL_COUNTRY_URL = 'all?fields=name;alpha3Code';

    axios.get(ALL_COUNTRY_URL).then(response => {
      this.setState({countries: response.data})
    });
  }

  render() {
    return (
      <section>
        <div className="listCountry">
          <p className='List'>Countries List:</p>
          {this.state.countries.map((item, index) => (
            <Country
              key={index}
              listcountry={item.name}
              clicked={() => this.postSelectedHandler(item.alpha3Code)}
            />
          ))}
        </div>
        <InfoCountry
          code={this.state.selectedCountryCode}
        />
      </section>
    );
  }
}

export default ListCountries;