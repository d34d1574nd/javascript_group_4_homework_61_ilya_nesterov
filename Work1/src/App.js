import React, { Component } from 'react';
import './App.css';
import ListCountries from "./containers/ListCountries/ListCountries";


class App extends Component {
  render() {
    return (
      <div className="App">
        <ListCountries/>
      </div>
    );
  }
}

export default App;
