import React, {Component} from 'react';
import './Country.css';

class CountryInfo extends Component {

  render() {
    return (
      <div>
        <p className="country"  onClick={this.props.clicked}>{this.props.listcountry}</p>
      </div>
    );
  }
}

export default CountryInfo;