import React, {Component} from 'react';
import './InfoCountry.css';
import axios from "axios";

class InfoCountry extends Component {
  state = {
    country: null,
    borders: [],
  };

  componentDidUpdate(prevProps) {
    const COUNTRY_URL = 'alpha/';

    if (this.props.code) {
      if(prevProps.code !== this.props.code) {
        axios.get(COUNTRY_URL + this.props.code).then(response => {
          this.setState({country: response.data});
          if (response.data.borders.length !== 0) {
            const copyBorder = [];
            return Promise.all (response.data.borders.map(border => {
              return axios.get(COUNTRY_URL + border).then(response => {
                const names = {name: response.data.name};
                copyBorder.push(names);
                this.setState({borders: copyBorder});
              })
            }));
          } else {
            this.setState({borders: []})
          }
        }).catch(error => {
          console.log(error);
        });
      }
    }
  };

  render() {
    if (!this.state.country) return null;

    const Border = this.state.borders.map((item, index) => {
      return (<li className='namesBorderCountry' key={index}>{item.name}</li>)
    });

    return (
      <div className='InfoCountries'>
        <p className='List'>Country Information:</p>
          <div className='info'>
            <p><strong>Country name: </strong>{this.state.country.name}</p>
            <p><strong>Capital: </strong>{this.state.country.capital}</p>
            <p><strong>Numeric Code: </strong>{this.state.country.numericCode}</p>
            <p><strong>Population: </strong> {this.state.country.population} people</p>
            <hr/>
            <p><strong>Region: </strong>{this.state.country.region}</p>
            <p><strong>Subregion: </strong>{this.state.country.subregion}</p>
            <hr/>
            <p><strong>Border with: </strong></p>
            <ul>
              {Border}
            </ul>
          </div>
        <img className='flag' src={this.state.country.flag} alt={this.state.country.name}/>
      </div>
    );
  }
}

export default InfoCountry;